package onlineshopping.uz.exception;

public class ProductCreateException extends RuntimeException {

    public ProductCreateException(String message) {
        super(message);
    }
}

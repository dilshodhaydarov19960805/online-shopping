package onlineshopping.uz.exception;

public class PasswordLengthSmallException extends RuntimeException {

    public PasswordLengthSmallException(String message) {
        super(message);
    }

}

package onlineshopping.uz.exception;

public class UserNameAlreadyException extends RuntimeException {

    public UserNameAlreadyException(String message) {
        super(message);
    }

}

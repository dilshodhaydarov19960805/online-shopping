package onlineshopping.uz.exception.controller;

import onlineshopping.uz.exception.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import onlineshopping.uz.model.ErrorResponse;

import java.util.Date;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(value = UserNameAlreadyException.class)
    public HttpEntity<ErrorResponse> userNameException(UserNameAlreadyException exception) {
        String errorMessage = exception.getMessage();
        return new ResponseEntity<>(new ErrorResponse(
            errorMessage,
            new Date(),
                null
        ), HttpStatus.ALREADY_REPORTED);
    }

    @ExceptionHandler(value = PhoneAlreadyException.class)
    public HttpEntity<ErrorResponse> phoneAlreadyException(PhoneAlreadyException exception) {
        String errorMessage = exception.getMessage();
        return new ResponseEntity<>(new ErrorResponse(
                errorMessage,
                new Date(),
                null
        ), HttpStatus.ALREADY_REPORTED);
    }

    @ExceptionHandler(value = PasswordLengthSmallException.class)
    public HttpEntity<ErrorResponse> passwordLengthSmallException(PasswordLengthSmallException exception) {
        String errorMessage = exception.getMessage();
        return new ResponseEntity<>(new ErrorResponse(
           errorMessage,
           new Date(),
           null
        ), HttpStatus.LENGTH_REQUIRED);
    }

    @ExceptionHandler(value = BasketNotFoundException.class)
    public HttpEntity<ErrorResponse> basketNotFoundException(BasketNotFoundException exception) {
        String errorMessage = exception.getMessage();
        return new ResponseEntity<>(new ErrorResponse(
                errorMessage,
                new Date(),
                null
        ), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = CategoryNotFoundException.class)
    public HttpEntity<ErrorResponse> categoryNotFoundException(CategoryNotFoundException exception) {
        String errorMessage = exception.getMessage();
        return new ResponseEntity<>(new ErrorResponse(
                errorMessage,
                new Date(),
                null
        ), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = DistrictNotFoundException.class)
    public HttpEntity<ErrorResponse> districtNotFoundException(DistrictNotFoundException exception) {
        String errorMessage = exception.getMessage();
        return new ResponseEntity<>(new ErrorResponse(
                errorMessage,
                new Date(),
                null
        ), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ModelNotFoundException.class)
    public HttpEntity<ErrorResponse> modelNotFoundException(ModelNotFoundException exception) {
        String errorMessage = exception.getMessage();
        return new ResponseEntity<>(new ErrorResponse(
                errorMessage,
                new Date(),
                null
        ), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ProductCreateException.class)
    public HttpEntity<ErrorResponse> productCreateException(ProductCreateException exception) {
        String errorMessage = exception.getMessage();
        return new ResponseEntity<>(new ErrorResponse(
                errorMessage,
                new Date(),
                null
        ), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ProductNotFoundException.class)
    public HttpEntity<ErrorResponse> productNotFoundException(ProductNotFoundException exception) {
        String errorMessage = exception.getMessage();
        return new ResponseEntity<>(new ErrorResponse(
                errorMessage,
                new Date(),
                null
        ), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ProductUpdateException.class)
    public HttpEntity<ErrorResponse> productUpdateException(ProductUpdateException exception) {
        String errorMessage = exception.getMessage();
        return new ResponseEntity<>(new ErrorResponse(
                errorMessage,
                new Date(),
                null
        ), HttpStatus.BAD_REQUEST);
    }

}

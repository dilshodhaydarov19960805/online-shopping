package onlineshopping.uz.exception;

public class ProductUpdateException extends RuntimeException {

    public ProductUpdateException(String message) {
        super(message);
    }

}

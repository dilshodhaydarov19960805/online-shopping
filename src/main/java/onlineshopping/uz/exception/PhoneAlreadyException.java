package onlineshopping.uz.exception;

public class PhoneAlreadyException extends RuntimeException {

    public PhoneAlreadyException(String message) {
        super(message);
    }

}

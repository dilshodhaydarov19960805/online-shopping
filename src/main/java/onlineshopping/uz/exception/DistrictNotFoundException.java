package onlineshopping.uz.exception;

public class DistrictNotFoundException extends RuntimeException {

    public DistrictNotFoundException(String message) {
        super(message);
    }
}

package onlineshopping.uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import onlineshopping.uz.model.domain.Product;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query(value = "select p from product p where category_id = :categoryId", nativeQuery = true)
    List<Product> findAllCategoryId(@Param("categoryId") Long categoryId);

}

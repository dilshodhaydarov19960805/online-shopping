package onlineshopping.uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import onlineshopping.uz.model.domain.Model;

import java.util.List;

@Repository
public interface ModelRepository extends JpaRepository<Model, Long> {

    @Query(value = "select m from model m where product_id = :product_id",nativeQuery = true)
    List<Model> findAllByProductId(@Param("product_id") Long productId);
}

package onlineshopping.uz.repository;

import onlineshopping.uz.model.domain.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import onlineshopping.uz.model.domain.Users;

import java.util.List;
import java.util.Optional;

public interface UsersRepository extends JpaRepository<Users, Long> {

    @Query("SELECT s from Users s where s.email = ?1")
    Optional<Users> findUsersByEmail(String email);

    Users findByPhone(String phone);

    List<Users> findAllByRegionId(Long id);

    List<Users> findAllByDistrictId(Long id);
}

package onlineshopping.uz.repository;

import onlineshopping.uz.model.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import onlineshopping.uz.model.domain.Region;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {

}

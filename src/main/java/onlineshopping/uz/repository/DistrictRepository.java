package onlineshopping.uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import onlineshopping.uz.model.domain.District;

import java.util.List;

public interface DistrictRepository extends JpaRepository<District, Long> {

    void deleteByRegionId(Long id);

    List<District> findAllById(Long id);

}

package onlineshopping.uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import onlineshopping.uz.model.domain.Region;

@Repository
public interface RegionRepository extends JpaRepository<Region, Long> {

}

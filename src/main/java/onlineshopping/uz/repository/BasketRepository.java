package onlineshopping.uz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import onlineshopping.uz.model.domain.Basket;

import java.util.List;

@Repository
public interface BasketRepository extends JpaRepository<Basket, Long> {

    @Query(value = "select b from basket b where user_id = :user_id",nativeQuery = true)
    List<Basket> findAllByUserId(@Param("user_id") Long userId);

}

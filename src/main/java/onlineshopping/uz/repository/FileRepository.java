package onlineshopping.uz.repository;

import onlineshopping.uz.model.domain.Model;
import onlineshopping.uz.model.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import onlineshopping.uz.model.domain.File;

import java.util.List;

@Repository
public interface FileRepository extends JpaRepository<File, Long> {

    @Query("select f.id from File f where f.model = :model")
    List<Long> findAllByModelId(@Param("model") Model model);

    @Query("select f.id from File f where f.product = :product")
    List<Long> findByProductId(@Param("product") Product product);
}

package onlineshopping.uz.model;


import onlineshopping.uz.model.CustomError;

import java.util.Date;
import java.util.List;

public class ErrorResponse {

    private String message;
    private Date timestamp;
    private List<CustomError> errors;

    public ErrorResponse(String errorMessage, Date date, List<CustomError> errore) {
        this.message = errorMessage;
        this.timestamp = date;
        this.errors = errore;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public List<CustomError> getErrors() {
        return errors;
    }

    public void setErrors(List<CustomError> errors) {
        this.errors = errors;
    }
}

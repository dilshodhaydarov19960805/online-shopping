package onlineshopping.uz.model.domain;

import onlineshopping.uz.model.dto.DistrictDto;
import onlineshopping.uz.utils.CustomUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "district")
public class District implements Serializable {

    @Id
    @SequenceGenerator(
            name = "sequence_district_id",
            sequenceName = "sequence_district_id",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "sequence_district_id",
            strategy = GenerationType.SEQUENCE
    )
    @Column(name = "id")
    private Long id;

    @Column(name = "district_name_uz")
    private String districtNameUz;

    @Column(name = "district_name_ru")
    private String districtNameRu;

    @Column(name = "district_name_en")
    private String districtNameEn;

    @ManyToOne
    @JoinColumn(name = "region_id")
    private Region region;

    @OneToMany(mappedBy = "region", targetEntity=Users.class, cascade=CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<Users> users;

    public District(Long districtId) {
        this.id = districtId;
    }

    public District() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDistrictNameUz() {
        return districtNameUz;
    }

    public void setDistrictNameUz(String districtNameUz) {
        this.districtNameUz = districtNameUz;
    }

    public String getDistrictNameRu() {
        return districtNameRu;
    }

    public void setDistrictNameRu(String districtNameRu) {
        this.districtNameRu = districtNameRu;
    }

    public String getDistrictNameEn() {
        return districtNameEn;
    }

    public void setDistrictNameEn(String districtNameEn) {
        this.districtNameEn = districtNameEn;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Set<Users> getUsers() {
        return users;
    }

    public void setUsers(Set<Users> users) {
        this.users = users;
    }

    public DistrictDto map2Dto() {

        DistrictDto dto = new DistrictDto();
        if (this.id != null && this.id > 0)
            dto.setDistrictId(this.id);

        if (CustomUtils.isNotNullEmpty(this.districtNameEn))
            dto.setDistrictNameEn(this.districtNameEn);

        if (CustomUtils.isNotNullEmpty(this.districtNameRu))
            dto.setDistrictNameRu(this.districtNameRu);

        if (CustomUtils.isNotNullEmpty(this.districtNameUz))
            dto.setDistrictNameUz(this.districtNameUz);

        if (this.region != null)
            dto.setRegionId(this.region.getId());

        return dto;
    }
}

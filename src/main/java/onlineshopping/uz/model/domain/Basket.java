package onlineshopping.uz.model.domain;

import onlineshopping.uz.model.dto.BasketDto;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "basket")
public class    Basket implements Serializable {

    @Id
    @SequenceGenerator(
            name = "sequence_basket_id",
            sequenceName = "sequence_basket_id",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "sequence_basket_id",
            strategy = GenerationType.SEQUENCE
    )
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "model_id",insertable = false)
    private Model model;

    @ManyToOne
    @JoinColumn(name = "user_id",insertable = false)
    private Users users;

    @Column(name = "count")
    private Integer count;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Users getUsers() {
        return users;
    }

    public void setUsers(Users users) {
        this.users = users;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public BasketDto map2Dto() {
        BasketDto dto = new BasketDto();

        if (this.id != null && this.id > 0)
            dto.setBasketId(this.id);

        if (this.count != null && this.count > 0)
            dto.setBasketCount(this.count);

        if (this.model != null)
            dto.setModelId(model.getId());

        if (this.users != null)
            dto.setUserId(this.users.getId());

            return dto;

    }

}

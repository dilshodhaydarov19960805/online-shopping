package onlineshopping.uz.model.domain;

import onlineshopping.uz.model.dto.CategoryDto;
import onlineshopping.uz.utils.CustomUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "category")
public class Category implements Serializable {

    @Id
    @SequenceGenerator(
            name = "sequence_category_id",
            sequenceName = "sequence_category_id",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "sequence_category_id",
            strategy = GenerationType.SEQUENCE
    )
    @Column(name = "category_id")
    private Long id;

    @Column(name = "category_name_uz")
    private String categoryNameUz;

    @Column(name = "category_name_ru")
    private String categoryNameRu;

    @Column(name = "category_name_en")
    private String categoryNameEn;

    @OneToMany(mappedBy = "category", fetch = FetchType.LAZY, targetEntity = Product.class, cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Set<Product> products;

    public Category(Long categoryId) {
        this.id = categoryId;
    }

    public Category() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryNameUz() {
        return categoryNameUz;
    }

    public void setCategoryNameUz(String categoryNameUz) {
        this.categoryNameUz = categoryNameUz;
    }

    public String getCategoryNameRu() {
        return categoryNameRu;
    }

    public void setCategoryNameRu(String categoryNameRu) {
        this.categoryNameRu = categoryNameRu;
    }

    public String getCategoryNameEn() {
        return categoryNameEn;
    }

    public void setCategoryNameEn(String categoryNameEn) {
        this.categoryNameEn = categoryNameEn;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }

    public CategoryDto map2Dto() {

        CategoryDto categoryDto = new CategoryDto();

        if (this.id != null && this.id > 0)
            categoryDto.setId(this.id);

        if (CustomUtils.isNotNullEmpty(this.categoryNameEn))
            categoryDto.setCategoryNameEn(this.categoryNameEn);

        if (CustomUtils.isNotNullEmpty(this.categoryNameRu))
            categoryDto.setCategoryNameEn(this.categoryNameRu);

        if (CustomUtils.isNotNullEmpty(this.categoryNameUz))
            categoryDto.setCategoryNameEn(this.categoryNameUz);

        return categoryDto;
    }
}

package onlineshopping.uz.model.domain;

import onlineshopping.uz.model.dto.ModelDto;
import onlineshopping.uz.utils.CustomUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "model")
public class Model implements Serializable {

    @Id
    @SequenceGenerator(
            name = "sequence_model_id",
            sequenceName = "sequence_model_id",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "sequence_model_id",
            strategy = GenerationType.SEQUENCE
    )
    @Column(name = "id")
    private Long id;

    @Column(name = "model_name_uz")
    private String modelNameUz;

    @Column(name = "model_name_ru")
    private String modelNameRu;

    @Column(name = "model_name_en")
    private String modelNameEn;

    @Column(name = "price")
    private Long price;

    @Column(name = "new_price")
    private Long newPrice;

    @Column(name = "star_count")
    private Integer starCount;

    @OneToMany(mappedBy = "model", targetEntity=File.class, cascade=CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<File> files;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable=false)
    private Product product;

    @OneToMany(mappedBy = "model", targetEntity=Basket.class, cascade=CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Basket> baskets;

    public Model(Long modelId) {
        this.id = modelId;
    }

    public Model() {

    }

    public Set<File> getFiles() {
        return files;
    }

    public void setFiles(Set<File> files) {
        this.files = files;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModelNameUz() {
        return modelNameUz;
    }

    public void setModelNameUz(String modelNameUz) {
        this.modelNameUz = modelNameUz;
    }

    public String getModelNameRu() {
        return modelNameRu;
    }

    public void setModelNameRu(String modelNameRu) {
        this.modelNameRu = modelNameRu;
    }

    public String getModelNameEn() {
        return modelNameEn;
    }

    public void setModelNameEn(String modelNameEn) {
        this.modelNameEn = modelNameEn;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(Long newPrice) {
        this.newPrice = newPrice;
    }

    public Integer getStarCount() {
        return starCount;
    }

    public void setStarCount(Integer starCount) {
        this.starCount = starCount;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Set<Basket> getBaskets() {
        return baskets;
    }

    public void setBaskets(Set<Basket> baskets) {
        this.baskets = baskets;
    }

    public ModelDto map2Dto() {
        ModelDto model = new ModelDto();

        if (this.id != null && this.id > 0)
            model.setModelId(this.id);

        if (CustomUtils.isNotNullEmpty(this.modelNameEn))
            model.setModelNameEn(this.modelNameEn);

        if (CustomUtils.isNotNullEmpty(this.modelNameRu))
            model.setModelNameEn(this.modelNameRu);

        if (CustomUtils.isNotNullEmpty(this.modelNameUz))
            model.setModelNameEn(this.modelNameUz);

        if (this.price != null && this.price > 0)
            model.setModelPrice(this.price);

        if (this.newPrice != null && this.newPrice > 0)
            model.setModelNewPrice(this.newPrice);

        if (this.product != null)
            model.setProductId(this.product.getId());

        return null;

    }
}

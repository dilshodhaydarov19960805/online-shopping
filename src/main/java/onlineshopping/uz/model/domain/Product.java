package onlineshopping.uz.model.domain;

import onlineshopping.uz.model.dto.ProductDto;
import onlineshopping.uz.utils.CustomUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "product")
public class Product implements Serializable {

    @Id
    @SequenceGenerator(
            name = "sequence_product_id",
            sequenceName = "sequence_product_id",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "sequence_product_id",
            strategy = GenerationType.SEQUENCE
    )
    @Column(name = "id")
    private Long id;

    @Column(name = "product_name_uz")
    private String productNameUz;

    @Column(name = "product_name_ru")
    private String productNameRu;

    @Column(name = "product_name_en")
    private String productNameEn;

    @OneToMany(mappedBy = "product", targetEntity=File.class, cascade=CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<File> files;

    @ManyToOne
    @JoinColumn(name = "category_id", nullable=false)
    private Category category;

    @OneToMany(mappedBy = "product", targetEntity=Model.class,cascade=CascadeType.REMOVE, orphanRemoval = true, fetch = FetchType.LAZY)
    private Set<Model> model;

    public Product(Long productId) {
        this.id = productId;
    }

    public Product() {

    }

    public Set<File> getFiles() {
        return files;
    }

    public void setFiles(Set<File> files) {
        this.files = files;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductNameUz() {
        return productNameUz;
    }

    public void setProductNameUz(String productNameUz) {
        this.productNameUz = productNameUz;
    }

    public String getProductNameRu() {
        return productNameRu;
    }

    public void setProductNameRu(String productNameRu) {
        this.productNameRu = productNameRu;
    }

    public String getProductNameEn() {
        return productNameEn;
    }

    public void setProductNameEn(String productNameEn) {
        this.productNameEn = productNameEn;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Set<Model> getModel() {
        return model;
    }

    public void setModel(Set<Model> model) {
        this.model = model;
    }

    public ProductDto map2Dto() {
        ProductDto product = new ProductDto();
        if (this.id != null && this.id > 0)
            product.setProductId(this.id);

        if (CustomUtils.isNotNullEmpty(this.productNameEn))
            product.setProductNameEn(this.productNameEn);

        if (CustomUtils.isNotNullEmpty(this.productNameRu))
            product.setProductNameEn(this.productNameRu);

        if (CustomUtils.isNotNullEmpty(this.productNameUz))
            product.setProductNameEn(this.productNameUz);

        if (this.category != null)
            product.setCategoryId(this.category.getId());

        return product;
    }

}

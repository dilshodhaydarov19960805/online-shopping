package onlineshopping.uz.model.domain;

import onlineshopping.uz.model.dto.RegionDto;
import onlineshopping.uz.utils.CustomUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "region")
public class Region implements Serializable {

    @Id
    @SequenceGenerator(
            name = "sequence_region_id",
            sequenceName = "sequence_region_id",
            allocationSize = 1
    )
    @GeneratedValue(
            generator = "sequence_region_id",
            strategy = GenerationType.SEQUENCE
    )
    @Column(name = "id")
    private Long id;

    @Column(name = "region_name_uz")
    private String regionNameUz;

    @Column(name = "region_name_en")
    private String regionNameEn;

    @Column(name = "region_name_ru")
    private String regionNameRu;

    @OneToMany(mappedBy = "region", targetEntity=District.class,cascade=CascadeType.REMOVE, fetch = FetchType.LAZY)
    private Set<District> districts;

    @OneToMany(mappedBy = "region", targetEntity=Users.class, fetch = FetchType.LAZY)
    private Set<Users> users;

    public Region(Long regionId) {
        this.id = regionId;
    }

    public Region() {
    }

    public Set<District> getDistricts() {
        return districts;
    }

    public void setDistricts(Set<District> districts) {
        this.districts = districts;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegionNameUz() {
        return regionNameUz;
    }

    public void setRegionNameUz(String regionNameUz) {
        this.regionNameUz = regionNameUz;
    }

    public String getRegionNameEn() {
        return regionNameEn;
    }

    public void setRegionNameEn(String regionNameEn) {
        this.regionNameEn = regionNameEn;
    }

    public String getRegionNameRu() {
        return regionNameRu;
    }

    public void setRegionNameRu(String regionNameRu) {
        this.regionNameRu = regionNameRu;
    }

    public Set<Users> getUsers() {
        return users;
    }

    public void setUsers(Set<Users> users) {
        this.users = users;
    }

    public RegionDto map2Dto() {
        RegionDto regionDto = new RegionDto();
        if (this.id != null && this.id > 0 )
            regionDto.setRegionId(this.id);

        if (CustomUtils.isNotNullEmpty(this.regionNameEn))
            regionDto.setRegionNameEn(this.regionNameEn);

        if (CustomUtils.isNotNullEmpty(this.regionNameRu))
            regionDto.setRegionNameRu(this.regionNameRu);

        if (CustomUtils.isNotNullEmpty(this.regionNameUz))
            regionDto.setRegionNameUz(this.regionNameUz);

        return regionDto;
    }
}

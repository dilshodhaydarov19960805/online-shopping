package onlineshopping.uz.model.domain;

import onlineshopping.uz.model.domain.enurmation.Role;
import onlineshopping.uz.model.domain.enurmation.Status;
import onlineshopping.uz.model.dto.UsersDto;
import onlineshopping.uz.utils.CustomUtils;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.util.Set;

import static javax.persistence.GenerationType.SEQUENCE;

@Entity
@Table(
        name = "user_info",
        uniqueConstraints = {
                @UniqueConstraint(name = "users_email_unique",columnNames = "email")
        }
    )
public class Users implements Serializable {

    @Id
    @SequenceGenerator(
            name = "users_sequence",
            sequenceName = "users_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = SEQUENCE,
            generator = "users_sequence"
    )
    @Column(
            name = "id",
            nullable = false
    )
    private Long id;
    @Column(
            name = "first_name",
            nullable = false,
            columnDefinition = "text"
    )
    private String firstName;
    @Column(
            name = "last_name",
            nullable = false,
            columnDefinition = "text"
    )
    private String lastName;
    @Column(
            name = "email",
            nullable = false,
            columnDefinition = "text",
            unique = true
    )
    @Email
    @NotNull
    private String email;
    @Column(
            name = "password"
    )
    private String password;
    @Column(
            name = "phone",
            nullable = false,
            columnDefinition = "text",
            unique = true
    )
    @NotNull
    private String phone;
    @Column(
            name = "birth"
    )
    private LocalDate birth;
    @Column(
            name = "age"
    )
    @Transient
    private Integer age;

    @Enumerated(EnumType.STRING)
    private Status status;

    @Enumerated(EnumType.STRING)
    private Role role;

    @OneToMany(mappedBy = "users", targetEntity=Basket.class, cascade=CascadeType.REMOVE, fetch = FetchType.LAZY)
    private Set<Basket> baskets;

    @Column(name = "address")
    private String address;

    @ManyToOne
    @JoinColumn(
            name = "district_id"
    )
    private District district;

    @ManyToOne
    @JoinColumn(
            name = "region_id"
    )
    private Region region;

    public Users(String firstName, String lastName, String email, String phone, LocalDate birth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.birth = birth;
    }

    public Users(Long userId) {
        this.id = userId;
    }

    public Users() {
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public LocalDate getBirth() {
        return birth;
    }

    public void setBirth(LocalDate birth) {
        this.birth = birth;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getAge() {
        return Period.between(this.birth != null ?this.birth : LocalDate.now(), LocalDate.now()).getYears();
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Set<Basket> getBaskets() {
        return baskets;
    }

    public void setBaskets(Set<Basket> baskets) {
        this.baskets = baskets;
    }

    public UsersDto mapToDto() {
        UsersDto dto = new UsersDto();

        if (this.id != null && this.id > 0)
            dto.setId(this.id);

        if (CustomUtils.isNotNullEmpty(this.firstName))
            dto.setFirstName(this.firstName);

        if (CustomUtils.isNotNullEmpty(this.lastName))
            dto.setLastName(this.lastName);

        if (CustomUtils.isNotNullEmpty(this.email))
            dto.setEmail(this.email);

        if (CustomUtils.isNotNullEmpty(this.phone))
            dto.setPhone(this.phone);

        if (CustomUtils.isNotNullEmpty(this.password))
            dto.setPassword(this.password);

        if (this.birth != null)
            dto.setBirth(String.valueOf(this.birth));

        if (this.age != null)
            dto.setAge(this.age);

        if (this.role !=null)
            dto.setRole(this.role);

        if (this.status != null)
            dto.setStatus(this.status);

        if (this.region != null)
            dto.setRegionId(this.region.getId());

        if (this.district != null)
            dto.setDistrictId(this.district.getId());

        if (CustomUtils.isNotNullEmpty(this.address))
            dto.setAddress(this.address);

        return dto;

    }

}

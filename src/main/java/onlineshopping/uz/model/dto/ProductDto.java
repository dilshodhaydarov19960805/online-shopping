package onlineshopping.uz.model.dto;

import onlineshopping.uz.model.domain.Category;
import onlineshopping.uz.model.domain.Product;
import onlineshopping.uz.utils.CustomUtils;

import java.util.List;
import java.util.Objects;

public class ProductDto {

    private Long productId;
    private String productNameUz;
    private String productNameRu;
    private String productNameEn;
    private List<Long> files;
    private Long categoryId;

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public String getProductNameUz() {
        return productNameUz;
    }

    public void setProductNameUz(String productNameUz) {
        this.productNameUz = productNameUz;
    }

    public String getProductNameRu() {
        return productNameRu;
    }

    public void setProductNameRu(String productNameRu) {
        this.productNameRu = productNameRu;
    }

    public String getProductNameEn() {
        return productNameEn;
    }

    public void setProductNameEn(String productNameEn) {
        this.productNameEn = productNameEn;
    }

    public List<Long> getFiles() {
        return files;
    }

    public void setFiles(List<Long> files) {
        this.files = files;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductDto that = (ProductDto) o;
        return Objects.equals(productId, that.productId) && Objects.equals(productNameUz, that.productNameUz) && Objects.equals(productNameRu, that.productNameRu) && Objects.equals(productNameEn, that.productNameEn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productId, productNameUz, productNameRu, productNameEn);
    }

    public Product map2Entity() {

        Product product = new Product();

        if (this.productId != null && this.productId > 0)
            product.setId(this.productId);

        if (CustomUtils.isNotNullEmpty(this.productNameEn))
            product.setProductNameEn(this.productNameEn);

        if (CustomUtils.isNotNullEmpty(this.productNameRu))
            product.setProductNameEn(this.productNameRu);

        if (CustomUtils.isNotNullEmpty(this.productNameUz))
            product.setProductNameEn(this.productNameUz);

        if (this.categoryId != null && this.categoryId > 0)
            product.setCategory(new Category(this.categoryId));

        return product;
    }

}

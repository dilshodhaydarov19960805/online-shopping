package onlineshopping.uz.model.dto;

import onlineshopping.uz.model.domain.Basket;
import onlineshopping.uz.model.domain.Model;
import onlineshopping.uz.model.domain.Users;

import java.util.Objects;

public class BasketDto {

    private Long basketId;
    private Integer basketCount;
    private Long userId;
    private Long modelId;

    public Long getBasketId() {
        return basketId;
    }

    public void setBasketId(Long basketId) {
        this.basketId = basketId;
    }

    public Integer getBasketCount() {
        return basketCount;
    }

    public void setBasketCount(Integer basketCount) {
        this.basketCount = basketCount;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BasketDto basketDto = (BasketDto) o;
        return Objects.equals(basketId, basketDto.basketId) && Objects.equals(basketCount, basketDto.basketCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(basketId, basketCount);
    }

    @Override
    public String toString() {
        return "BasketDto{" +
                "basketId=" + basketId +
                ", basketCount=" + basketCount +
                ", usersDto=" + userId +
                ", modelDto=" + modelId +
                '}';
    }

    public Basket map2Entity() {
        Basket basket = new Basket();

        if (this.basketId != null && this.basketId > 0)
            basket.setId(this.basketId);

        if (this.basketCount != null && this.basketCount > 0)
            basket.setCount(this.basketCount);

        if (this.modelId != null && this.modelId > 0)
            basket.setModel(new Model(this.modelId));

        if (this.userId != null && this.userId > 0)
            basket.setUsers(new Users(this.userId));

        return basket;
    }

}

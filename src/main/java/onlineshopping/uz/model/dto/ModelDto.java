package onlineshopping.uz.model.dto;

import onlineshopping.uz.model.domain.Model;
import onlineshopping.uz.model.domain.Product;
import onlineshopping.uz.utils.CustomUtils;

import java.util.List;
import java.util.Objects;

public class ModelDto {

    private Long modelId;
    private String modelNameUz;
    private String modelNameRu;
    private String modelNameEn;
    private Long modelPrice;
    private Long modelNewPrice;
    private Integer modelStarCount;
    private Long productId;
    private List<Long> fileList;

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public String getModelNameUz() {
        return modelNameUz;
    }

    public void setModelNameUz(String modelNameUz) {
        this.modelNameUz = modelNameUz;
    }

    public String getModelNameRu() {
        return modelNameRu;
    }

    public void setModelNameRu(String modelNameRu) {
        this.modelNameRu = modelNameRu;
    }

    public String getModelNameEn() {
        return modelNameEn;
    }

    public void setModelNameEn(String modelNameEn) {
        this.modelNameEn = modelNameEn;
    }

    public Long getModelPrice() {
        return modelPrice;
    }

    public void setModelPrice(Long modelPrice) {
        this.modelPrice = modelPrice;
    }

    public Long getModelNewPrice() {
        return modelNewPrice;
    }

    public void setModelNewPrice(Long modelNewPrice) {
        this.modelNewPrice = modelNewPrice;
    }

    public Integer getModelStarCount() {
        return modelStarCount;
    }

    public void setModelStarCount(Integer modelStarCount) {
        this.modelStarCount = modelStarCount;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public List<Long> getFileList() {
        return fileList;
    }

    public void setFileList(List<Long> fileList) {
        this.fileList = fileList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModelDto modelDto = (ModelDto) o;
        return Objects.equals(modelId, modelDto.modelId) && Objects.equals(modelNameUz, modelDto.modelNameUz) && Objects.equals(modelNameRu, modelDto.modelNameRu) && Objects.equals(modelNameEn, modelDto.modelNameEn) && Objects.equals(modelPrice, modelDto.modelPrice) && Objects.equals(modelNewPrice, modelDto.modelNewPrice) && Objects.equals(modelStarCount, modelDto.modelStarCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(modelId, modelNameUz, modelNameRu, modelNameEn, modelPrice, modelNewPrice, modelStarCount);
    }

    @Override
    public String toString() {
        return "ModelDto{" +
                "modelId=" + modelId +
                ", modelNameUz='" + modelNameUz + '\'' +
                ", modelNameRu='" + modelNameRu + '\'' +
                ", modelNameEn='" + modelNameEn + '\'' +
                ", modelPrice=" + modelPrice +
                ", modelNewPrice=" + modelNewPrice +
                ", modelStarCount=" + modelStarCount +
                ", productDto=" + productId +
                ", fileDtos=" + fileList +
                '}';
    }

    public Model map2Entity() {
        Model model = new Model();

        if (this.modelId != null && this.modelId > 0)
            model.setId(this.modelId);

        if (CustomUtils.isNotNullEmpty(this.modelNameEn))
            model.setModelNameEn(this.modelNameEn);

        if (CustomUtils.isNotNullEmpty(this.modelNameRu))
            model.setModelNameEn(this.modelNameRu);

        if (CustomUtils.isNotNullEmpty(this.modelNameUz))
            model.setModelNameEn(this.modelNameUz);

        if (this.modelPrice != null && this.modelPrice > 0)
            model.setPrice(this.modelPrice);

        if (this.modelNewPrice != null && this.modelNewPrice > 0)
            model.setNewPrice(this.modelNewPrice);

        if (this.productId != null && this.productId > 0)
            model.setProduct(new Product(this.productId));

        return model;
    }
}

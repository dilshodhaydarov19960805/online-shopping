package onlineshopping.uz.model.dto;

import onlineshopping.uz.model.domain.Category;
import onlineshopping.uz.utils.CustomUtils;

import java.util.Objects;

public class CategoryDto {

    private Long id;
    private String categoryNameUz;
    private String categoryNameRu;
    private String categoryNameEn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryNameUz() {
        return categoryNameUz;
    }

    public void setCategoryNameUz(String categoryNameUz) {
        this.categoryNameUz = categoryNameUz;
    }

    public String getCategoryNameRu() {
        return categoryNameRu;
    }

    public void setCategoryNameRu(String categoryNameRu) {
        this.categoryNameRu = categoryNameRu;
    }

    public String getCategoryNameEn() {
        return categoryNameEn;
    }

    public void setCategoryNameEn(String categoryNameEn) {
        this.categoryNameEn = categoryNameEn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryDto that = (CategoryDto) o;
        return Objects.equals(id, that.id) && Objects.equals(categoryNameUz, that.categoryNameUz) && Objects.equals(categoryNameRu, that.categoryNameRu) && Objects.equals(categoryNameEn, that.categoryNameEn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, categoryNameUz, categoryNameRu, categoryNameEn);
    }

    @Override
    public String toString() {
        return "CategoryDto{" +
                "id=" + id +
                ", categoryNameUz='" + categoryNameUz + '\'' +
                ", categoryNameRu='" + categoryNameRu + '\'' +
                ", categoryNameEn='" + categoryNameEn + '\'' +
                '}';
    }

    public Category map2Entity() {
        Category category = new Category();

        if (this.id != null && this.id > 0)
            category.setId(this.id);

        if (CustomUtils.isNotNullEmpty(this.categoryNameEn))
            category.setCategoryNameEn(this.categoryNameEn);

        if (CustomUtils.isNotNullEmpty(this.categoryNameRu))
            category.setCategoryNameEn(this.categoryNameRu);

        if (CustomUtils.isNotNullEmpty(this.categoryNameUz))
            category.setCategoryNameEn(this.categoryNameUz);

        return category;
    }

}

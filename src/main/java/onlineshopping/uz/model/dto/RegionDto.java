package onlineshopping.uz.model.dto;

import onlineshopping.uz.model.domain.Region;
import onlineshopping.uz.utils.CustomUtils;

import java.util.Objects;

public class RegionDto {

    private Long regionId;
    private String regionNameUz;
    private String regionNameRu;
    private String regionNameEn;

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public String getRegionNameUz() {
        return regionNameUz;
    }

    public void setRegionNameUz(String regionNameUz) {
        this.regionNameUz = regionNameUz;
    }

    public String getRegionNameRu() {
        return regionNameRu;
    }

    public void setRegionNameRu(String regionNameRu) {
        this.regionNameRu = regionNameRu;
    }

    public String getRegionNameEn() {
        return regionNameEn;
    }

    public void setRegionNameEn(String regionNameEn) {
        this.regionNameEn = regionNameEn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RegionDto regionDto = (RegionDto) o;
        return Objects.equals(regionId, regionDto.regionId) && Objects.equals(regionNameUz, regionDto.regionNameUz) && Objects.equals(regionNameRu, regionDto.regionNameRu) && Objects.equals(regionNameEn, regionDto.regionNameEn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(regionId, regionNameUz, regionNameRu, regionNameEn);
    }

    @Override
    public String toString() {
        return "RegionDto{" +
                "regionId=" + regionId +
                ", regionNameUz='" + regionNameUz + '\'' +
                ", regionNameRu='" + regionNameRu + '\'' +
                ", regionNameEn='" + regionNameEn + '\'' +
                '}';
    }

    public Region map2Entity() {
        Region region = new Region();

        if (this.regionId != null && this.regionId > 0 )
            region.setId(regionId);

        if (CustomUtils.isNotNullEmpty(this.regionNameEn))
            region.setRegionNameEn(this.regionNameEn);

        if (CustomUtils.isNotNullEmpty(this.regionNameRu))
            region.setRegionNameRu(this.regionNameRu);

        if (CustomUtils.isNotNullEmpty(this.regionNameUz))
            region.setRegionNameUz(this.regionNameUz);

        return region;
    }
}

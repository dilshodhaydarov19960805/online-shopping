package onlineshopping.uz.model.dto;

import onlineshopping.uz.model.domain.District;
import onlineshopping.uz.model.domain.Region;
import onlineshopping.uz.model.domain.Users;
import onlineshopping.uz.model.domain.enurmation.Role;
import onlineshopping.uz.model.domain.enurmation.Status;
import onlineshopping.uz.utils.CustomUtils;

import java.time.LocalDate;
import java.util.Objects;

public class UsersDto {

    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String phone;
    private String birth;
    private Integer age;
    private Status status;
    private Role role;
    private String address;
    private Long regionId;
    private Long districtId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Users mapToEntity() {
        Users user = new Users();

        if (this.id != null && this.id > 0)
            user.setId(this.id);

        if (CustomUtils.isNotNullEmpty(this.firstName))
            user.setFirstName(this.firstName);

        if (CustomUtils.isNotNullEmpty(this.lastName))
            user.setLastName(this.lastName);

        if (CustomUtils.isNotNullEmpty(this.email))
            user.setEmail(this.email);

        if (CustomUtils.isNotNullEmpty(this.phone))
            user.setPhone(this.phone);

        if (CustomUtils.isNotNullEmpty(this.password))
            user.setPassword(this.password);

        if (CustomUtils.isNotNullEmpty(this.birth))
            user.setBirth(LocalDate.parse(birth));

        if (this.age != null)
            user.setAge(this.age);

        if (this.role !=null)
            user.setRole(this.role);

        if (this.status != null)
            user.setStatus(this.status);

        if (this.regionId != null && this.regionId > 0)
            user.setRegion(new Region(this.regionId));

        if (this.districtId != null && this.districtId > 0)
            user.setDistrict(new District(this.districtId));

        if (CustomUtils.isNotNullEmpty(this.address))
            user.setAddress(this.address);

        return user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsersDto usersDto = (UsersDto) o;
        return Objects.equals(id, usersDto.id) && Objects.equals(firstName, usersDto.firstName) && Objects.equals(lastName, usersDto.lastName) && Objects.equals(email, usersDto.email) && Objects.equals(password, usersDto.password) && Objects.equals(phone, usersDto.phone) && Objects.equals(birth, usersDto.birth) && Objects.equals(age, usersDto.age) && status == usersDto.status && role == usersDto.role && Objects.equals(regionId, usersDto.regionId) && Objects.equals(districtId, usersDto.districtId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email, password, phone, birth, age, status, role, regionId, districtId);
    }

    @Override
    public String toString() {
        return "UsersDto{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", phone='" + phone + '\'' +
                ", birth='" + birth + '\'' +
                ", age=" + age +
                ", status=" + status +
                ", role=" + role +
                ", addressId=" + address +
                ", regionId=" + regionId +
                ", districtId=" + districtId +
                '}';
    }
}

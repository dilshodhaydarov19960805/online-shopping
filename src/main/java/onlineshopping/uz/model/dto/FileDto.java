package onlineshopping.uz.model.dto;

import java.util.Objects;

public class FileDto {

    private Long fileId;
    private String fileName;
    private String fileType;
    private String fileContentType;
    private Double fileSize;

    public Long getFileId() {
        return fileId;
    }

    public void setFileId(Long fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public Double getFileSize() {
        return fileSize;
    }

    public void setFileSize(Double fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileDto fileDto = (FileDto) o;
        return Objects.equals(fileId, fileDto.fileId) && Objects.equals(fileName, fileDto.fileName) && Objects.equals(fileType, fileDto.fileType) && Objects.equals(fileContentType, fileDto.fileContentType) && Objects.equals(fileSize, fileDto.fileSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileId, fileName, fileType, fileContentType, fileSize);
    }

    @Override
    public String toString() {
        return "FileDto{" +
                "fileId=" + fileId +
                ", fileName='" + fileName + '\'' +
                ", fileType='" + fileType + '\'' +
                ", fileContentType='" + fileContentType + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }
}

package onlineshopping.uz.model.dto;

import onlineshopping.uz.model.domain.District;
import onlineshopping.uz.model.domain.Region;
import onlineshopping.uz.utils.CustomUtils;

import java.util.Objects;

public class DistrictDto {

    private Long districtId;
    private String districtNameUz;
    private String districtNameRu;
    private String districtNameEn;
    private Long regionId;

    public Long getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }

    public String getDistrictNameUz() {
        return districtNameUz;
    }

    public void setDistrictNameUz(String districtNameUz) {
        this.districtNameUz = districtNameUz;
    }

    public String getDistrictNameRu() {
        return districtNameRu;
    }

    public void setDistrictNameRu(String districtNameRu) {
        this.districtNameRu = districtNameRu;
    }

    public String getDistrictNameEn() {
        return districtNameEn;
    }

    public void setDistrictNameEn(String districtNameEn) {
        this.districtNameEn = districtNameEn;
    }

    public Long getRegionId() {
        return regionId;
    }

    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DistrictDto that = (DistrictDto) o;
        return Objects.equals(districtId, that.districtId) && Objects.equals(districtNameUz, that.districtNameUz) && Objects.equals(districtNameRu, that.districtNameRu) && Objects.equals(districtNameEn, that.districtNameEn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(districtId, districtNameUz, districtNameRu, districtNameEn);
    }

    public District map2Entity() {
        District district = new District();

        if (this.districtId != null && this.districtId > 0)
            district.setId(this.districtId);

        if (CustomUtils.isNotNullEmpty(this.districtNameEn))
            district.setDistrictNameEn(this.districtNameEn);

        if (CustomUtils.isNotNullEmpty(this.districtNameRu))
            district.setDistrictNameRu(this.districtNameRu);

        if (CustomUtils.isNotNullEmpty(this.districtNameUz))
            district.setDistrictNameUz(this.districtNameUz);

        if (this.regionId != null && this.regionId > 0)
            district.setRegion(new Region(this.regionId));

        return district;
    }
}

package onlineshopping.uz.service;


import org.springframework.http.ResponseEntity;
import onlineshopping.uz.model.domain.Users;
import onlineshopping.uz.model.UserLogin;
import onlineshopping.uz.model.dto.UsersDto;

import java.util.List;
import java.util.Locale;

public interface UsersService {

    UsersDto findById(Long id, Locale locale);

    boolean update(UsersDto usersDto, Long id, Locale locale);

    boolean delete(Long id, Locale locale);

    UsersDto findByEmail(String email, Locale locale);

    ResponseEntity<?> save(UsersDto usersDto);

    ResponseEntity<?> loginUser(UserLogin login, Locale locale);

    List<Users> getAll();

    List<UsersDto> findByRegionId(Long id);

    List<UsersDto> findByDistrictId(Long id);
}

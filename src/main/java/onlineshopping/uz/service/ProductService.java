package onlineshopping.uz.service;

import onlineshopping.uz.model.dto.ProductDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Locale;

public interface ProductService {

    ProductDto save(List<MultipartFile> files, ProductDto productDto, Locale locale);

    boolean update(List<MultipartFile> multipartFiles, ProductDto productDto, Long id, Locale locale);

    List<ProductDto> findAll();

    List<ProductDto> findAll(Long categoryId);

    boolean delete(Long productId, Locale locale);

//    boolean deleteByCategoryId(Long categoryId);

    ProductDto findById(Long id, Locale locale);
}

package onlineshopping.uz.service;

import onlineshopping.uz.model.dto.ModelDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Locale;

public interface ModelService {

    ModelDto findById(Long id, Locale locale);

    ModelDto save(ModelDto modelDto, List<MultipartFile> files, Locale locale);

    List<ModelDto> findAll();

    List<ModelDto> findAll(Long productId);

    ModelDto update(ModelDto modelDto,Long id, List<MultipartFile> files, Locale locale);

    boolean deleteByModel(Long modelId, Locale locale);

//    boolean deleteByProductId(Long productId);

}

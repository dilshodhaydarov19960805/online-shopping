package onlineshopping.uz.service;

import onlineshopping.uz.model.dto.DistrictDto;

import java.util.List;
import java.util.Locale;

public interface DistrictService {

    DistrictDto findById(Long Id, Locale locale);

    boolean save(DistrictDto dto, Locale locale);

    List<DistrictDto> findAll(Long regionId);

    boolean delete(Long id, Locale locale);

    boolean update(DistrictDto dto, Long id, Locale locale);

    void deleteByRegionId(Long id, Locale locale);

}

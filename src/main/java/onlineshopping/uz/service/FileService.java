package onlineshopping.uz.service;

import onlineshopping.uz.model.domain.Product;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Locale;

public interface FileService {

    void findById(Long id, HttpServletResponse response, Locale locale);

    List<Long> findAllByModelId(Long modelId, Locale locale);

    boolean delete(Long id, Locale locale);

    List<Long> save(Long productId, Long modelId, List<MultipartFile> multipartFiles, Locale locale);

    List<Long> findByProductId(Product product);
}

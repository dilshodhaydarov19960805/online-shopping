package onlineshopping.uz.service.impl;

import onlineshopping.uz.exception.DistrictNotFoundException;
import onlineshopping.uz.model.domain.District;
import onlineshopping.uz.model.dto.DistrictDto;
import onlineshopping.uz.model.dto.UsersDto;
import onlineshopping.uz.repository.DistrictRepository;
import onlineshopping.uz.service.UsersService;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import onlineshopping.uz.service.DistrictService;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service("districtService")
public class DistrictServiceImpl implements DistrictService {

    private final DistrictRepository districtRepository;
    private final MessageSource messageSource;
    private final UsersService usersService;

    public DistrictServiceImpl(DistrictRepository districtRepository, MessageSource messageSource, UsersService usersService) {
        this.districtRepository = districtRepository;
        this.messageSource = messageSource;
        this.usersService = usersService;
    }

    @Override
    public DistrictDto findById(Long id, Locale lang) {
        District district = districtRepository.findById(id).orElseThrow(()-> new DistrictNotFoundException(messageSource.getMessage("district.not.found.error", null, lang)));
        return district.map2Dto();
    }

    @Override
    @Transactional
    public boolean save(DistrictDto dto, Locale lang) {
        try {
            districtRepository.save(dto.map2Entity());
            return true;
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(e.getMessage());
        }
    }

    @Override
    public List<DistrictDto> findAll(Long regionId) {
        List<DistrictDto> districtDtos = new ArrayList<>();
        districtRepository.findAll().forEach(value->{
                    if (regionId != null && value != null && regionId.equals(value.getRegion().getId())) {
                        districtDtos.add(value.map2Dto());
                    }
                });
        return districtDtos;
    }

    @Override
    @Transactional
    public boolean delete(Long id, Locale lang) {
        boolean result = false;
        try {
            if (districtRepository.existsById(id)){
                usersService.findByDistrictId(id).forEach(value->{
                    value.setDistrictId(null);
                    usersService.update(value, value.getId(), lang);
                });
                districtRepository.deleteById(id);
                result = true;
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(e.getMessage());
        }
        return result;
    }

    @Override
    @Transactional
    public boolean update(DistrictDto dto, Long id, Locale lang) {
        boolean result = false;
        try {
            if (districtRepository.existsById(id)){
                districtRepository.save(dto.map2Entity());
                result = true;
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(e.getMessage());
        }
        return result;
    }

    @Override
    @Transactional
    public void deleteByRegionId(Long id, Locale lang) {
        try {
            usersService.findByRegionId(id).forEach(value->{
                value.setDistrictId(null);
                usersService.update(value, value.getId(), lang);
            });
            districtRepository.deleteByRegionId(id);
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(e.getMessage());
        }
    }

}

package onlineshopping.uz.service.impl;

import onlineshopping.uz.exception.BasketNotFoundException;
import onlineshopping.uz.model.dto.BasketDto;
import onlineshopping.uz.repository.BasketRepository;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import onlineshopping.uz.service.BasketService;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service("basketService")
public class BasketServiceImpl implements BasketService {

    private final BasketRepository basketRepository;
    private final MessageSource messageSource;

    public BasketServiceImpl(BasketRepository basketRepository, MessageSource messageSource) {
        this.basketRepository = basketRepository;
        this.messageSource = messageSource;
    }

    @Override
    public BasketDto findById(Long id, Locale lang) {
        return basketRepository.findById(id).orElseThrow(()-> new BasketNotFoundException(messageSource.getMessage("basket.not.found.error", null, lang))).map2Dto();
    }

    @Override
    @Transactional
    public ResponseEntity<?> save(BasketDto basket, Locale lang) {
        try {
            BasketDto basketDto = basketRepository.save(basket.map2Entity()).map2Dto();
            return new ResponseEntity<>(basketDto, HttpStatus.CREATED);
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(messageSource.getMessage("basket.create.error", null, lang));
        }
    }

    @Override
    public ResponseEntity<?> update(BasketDto basket, Long id, Locale lang) {
        try {
            BasketDto basketDto = basketRepository.save(basket.map2Entity()).map2Dto();
            return new ResponseEntity<>(basketDto, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(messageSource.getMessage("basket.update.error", null, lang));
        }
    }

    @Override
    @Transactional
    public boolean delete(Long id, Locale lang) {
        boolean result = false;
        try {
            BasketDto basket = findById(id, lang);
            if (basket != null && basket.getBasketId() > 0){
                basketRepository.deleteById(id);
                result = true;
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(messageSource.getMessage("basket.delete.error", null, lang));
        }
        return result;
    }

    @Override
    public List<BasketDto> findAll(Long userId) {
        List<BasketDto> list = new ArrayList<>();
        basketRepository.findAllByUserId(userId).forEach(value->{
            list.add(value.map2Dto());
        });
        return list;
    }

}

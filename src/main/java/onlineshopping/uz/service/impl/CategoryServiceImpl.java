package onlineshopping.uz.service.impl;

import onlineshopping.uz.exception.CategoryNotFoundException;
import onlineshopping.uz.model.dto.CategoryDto;
import onlineshopping.uz.repository.CategoryRepository;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import onlineshopping.uz.service.CategoryService;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final MessageSource messageSource;

    public CategoryServiceImpl(CategoryRepository categoryRepository, MessageSource messageSource) {
        this.categoryRepository = categoryRepository;
        this.messageSource = messageSource;
    }

    @Override
    @Transactional
    public boolean save(CategoryDto category, Locale lang) {
        try {
            categoryRepository.save(category.map2Entity());
            return true;
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(messageSource.getMessage("category.create.error", null, lang));
        }
    }

    @Override
    @Transactional
    public boolean update(CategoryDto categoryDto, Long id, Locale lang) {
        try {
            if (categoryRepository.existsById(id)){
                categoryRepository.save(categoryDto.map2Entity());
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(messageSource.getMessage("category.update.error", null, lang));
        }
        return false;
    }

    @Override
    @Transactional
    public boolean delete(Long id, Locale lang) {
        try {
            if (categoryRepository.existsById(id)){
                categoryRepository.deleteById(id);
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(messageSource.getMessage("category.delete.error", null, lang));
        }
        return false;
    }

    @Override
    public List<CategoryDto> findAll() {
        List<CategoryDto> list = new ArrayList<>();
        categoryRepository.findAll().forEach(value->
                list.add(value.map2Dto())
                );
        return list;
    }

    @Override
    public CategoryDto findById(Long id, Locale locale) {
        return categoryRepository.findById(id).orElseThrow(()-> new CategoryNotFoundException(messageSource.getMessage("category.not.found.error", null, locale))).map2Dto();
    }

}

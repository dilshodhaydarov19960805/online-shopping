package onlineshopping.uz.service.impl;

import onlineshopping.uz.exception.ProductCreateException;
import onlineshopping.uz.exception.ProductNotFoundException;
import onlineshopping.uz.exception.ProductUpdateException;
import onlineshopping.uz.model.dto.ProductDto;
import onlineshopping.uz.repository.ProductRepository;
import onlineshopping.uz.service.FileService;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import onlineshopping.uz.service.ProductService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service("productService")
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final MessageSource messageSource;
    private final FileService fileService;

    public ProductServiceImpl(ProductRepository productRepository, MessageSource messageSource, FileService fileService) {
        this.productRepository = productRepository;
        this.messageSource = messageSource;
        this.fileService = fileService;
    }

    @Override
    @Transactional
    public ProductDto save(List<MultipartFile> files, ProductDto productDto, Locale lang) {
        ProductDto productDto1;
        try {
            productDto1 = productRepository.save(productDto.map2Entity()).map2Dto();
            if (productDto1 != null) {
                productDto1.setFiles(fileService.save(productDto1.getProductId(), null, files, lang));
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new ProductCreateException(messageSource.getMessage("product.create.error", null, lang));
        }
        return productDto1;
    }

    @Override
    @Transactional
    public boolean update(List<MultipartFile> multipartFiles, ProductDto productDto, Long id, Locale lang) {

        try {
            if (productRepository.existsById(productDto.getProductId())){
                productRepository.save(productDto.map2Entity()).map2Dto();
                fileService.save(productDto.getProductId(),null, multipartFiles, lang);
            }else
                throw new ProductUpdateException(messageSource.getMessage("product.not.found.error", null, lang));
        }catch (Exception e){
            e.printStackTrace();
            throw new ProductCreateException(e.getMessage());
        }
        return true;
    }

    @Override
    public List<ProductDto> findAll() {
        List<ProductDto> list = new ArrayList<>();
        productRepository.findAll().forEach(value->
                list.add(value.map2Dto())
                );
        return list;
    }

    @Override
    public List<ProductDto> findAll(Long categoryId) {
        List<ProductDto> list = new ArrayList<>();
        productRepository.findAllCategoryId(categoryId).forEach(value->
                list.add(value.map2Dto())
                );

        return list;
    }

    @Override
    @Transactional
    public boolean delete(Long productId, Locale lang) {

        try {
            if (productRepository.existsById(productId)){
                productRepository.deleteById(productId);
            }else
                throw new ProductNotFoundException(messageSource.getMessage("product.not.found.error", null, lang));
        }catch (Exception e){
            e.printStackTrace();
            throw new ProductNotFoundException(e.getMessage());
        }
        return true;
    }

    @Override
    public ProductDto findById(Long id, Locale lang) {
        ProductDto dto = productRepository.findById(id).orElseThrow(()-> new ProductNotFoundException(messageSource.getMessage("product.not.found.error", null, lang))).map2Dto();
        dto.setFiles(fileService.findByProductId(dto.map2Entity()));
        return dto;
    }

}

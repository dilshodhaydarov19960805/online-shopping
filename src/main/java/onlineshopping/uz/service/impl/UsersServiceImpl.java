package onlineshopping.uz.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import onlineshopping.uz.exception.PasswordLengthSmallException;
import onlineshopping.uz.exception.PhoneAlreadyException;
import onlineshopping.uz.exception.UserNameAlreadyException;
import onlineshopping.uz.model.domain.Users;
import onlineshopping.uz.model.UserLogin;
import onlineshopping.uz.model.dto.UsersDto;
import onlineshopping.uz.repository.UsersRepository;
import onlineshopping.uz.security.JwtTokenProvider;
import onlineshopping.uz.service.UsersService;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.transaction.Transactional;
import java.util.*;

@Slf4j
@Service("usersService")
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;
    private final MessageSource messageSource;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;

    public UsersServiceImpl(UsersRepository usersRepository, MessageSource messageSource, AuthenticationManager authenticationManager, JwtTokenProvider jwtTokenProvider) {
        this.usersRepository = usersRepository;
        this.messageSource = messageSource;
        this.authenticationManager = authenticationManager;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public UsersDto findById(Long id, Locale locale) {
        Users user = usersRepository.findById(id).orElseThrow(()-> new UsernameNotFoundException(messageSource.getMessage("user.not.found.error", null, locale)));
        return user.mapToDto();
    }

    @Override
    public boolean update(UsersDto usersDto, Long id, Locale lang) {
        boolean result;
        try {
            if (findById(id, lang) != null){
                Users user = usersDto.mapToEntity();
                usersRepository.save(user);
                result = true;
            }else
                result = false;
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(messageSource.getMessage("user.update.error", null, lang));
        }
        return result;
    }

    @Override
    public boolean delete(Long id, Locale lang) {
        boolean result;
        UsersDto user = findById(id, lang);
        try {
            if (user != null){
                usersRepository.deleteById(id);
                result = true;
            }else
                result = false;
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(messageSource.getMessage("user.delete.error", null, lang));
        }
        return result;
    }

    @Override
    public UsersDto findByEmail(String email, Locale lang) {
        Users optional = usersRepository.findUsersByEmail(email).orElseThrow(()-> new UsernameNotFoundException(messageSource.getMessage("email.not.found.error", null, lang)));
        return optional.mapToDto();
    }

    @Override
    @Transactional
    public ResponseEntity<?> save(UsersDto usersDto) {

        try {
            if (!checkPasswordLength(usersDto.getPassword())) {
                throw new PasswordLengthSmallException(messageSource.getMessage("password.length.small.error", null, Locale.ENGLISH));
            }

            if (checkEmailAvailability(usersDto.getEmail())) {
                throw new UserNameAlreadyException(messageSource.getMessage("email.already.error", null, Locale.ENGLISH));
            }

            if (!checkPhoneAvailability(usersDto.getPhone())) {
                throw new PhoneAlreadyException(messageSource.getMessage("phone.already.error", null, Locale.ENGLISH));
            }

            usersDto.setPassword(bCryptPasswordEncoder().encode(usersDto.getPassword()));
            Users dto = usersRepository.save(usersDto.mapToEntity());
            return new ResponseEntity<>(dto.mapToDto(), HttpStatus.CREATED);
        }catch (Exception e){
            e.printStackTrace();
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity<?> loginUser(UserLogin login, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login.getUserName(), login.getPassword()));
        UsersDto dto = findByEmail(login.getUserName(), locale);
        if (dto == null) {
            throw new UsernameNotFoundException(messageSource.getMessage("email.not.found.error", null, Locale.ENGLISH));
        }

        String token = jwtTokenProvider.createToken(dto.getEmail(), dto.getRole());
        Map<Object, Object> map = new HashMap<>();
        map.put("username", dto.getEmail());
        map.put("token", token);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @Override
    public List<Users> getAll() {
        List<Users> users = usersRepository.findAll();
        return users;
    }

    @Override
    public List<UsersDto> findByRegionId(Long id) {
        List<UsersDto> list = new ArrayList<>();
        usersRepository.findAllByRegionId(id).forEach(value->
                list.add(value.mapToDto())
                );
        return list;
    }

    @Override
    public List<UsersDto> findByDistrictId(Long id) {
        List<UsersDto> list = new ArrayList<>();
        usersRepository.findAllByDistrictId(id).forEach(value->
                list.add(value.mapToDto())
        );
        return list;
    }

    private boolean checkPhoneAvailability(String phone) {
        return usersRepository.findByPhone(phone) == null;
    }

    private boolean checkPasswordLength(String password) {
        return password.length() > 4;
    }

    private boolean checkEmailAvailability(String email) {
        return usersRepository.findUsersByEmail(email).isPresent();
    }

    BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

}

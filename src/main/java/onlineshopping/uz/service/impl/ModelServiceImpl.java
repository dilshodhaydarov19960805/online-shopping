package onlineshopping.uz.service.impl;

import onlineshopping.uz.exception.ModelNotFoundException;
import onlineshopping.uz.model.dto.ModelDto;
import onlineshopping.uz.repository.ModelRepository;
import onlineshopping.uz.service.FileService;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import onlineshopping.uz.service.ModelService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service("modelService")
public class ModelServiceImpl implements ModelService {

    private final ModelRepository modelRepository;
    private final MessageSource messageSource;
    private final FileService fileService;

    public ModelServiceImpl(ModelRepository modelRepository, MessageSource messageSource, FileService fileService) {
        this.modelRepository = modelRepository;
        this.messageSource = messageSource;
        this.fileService = fileService;
    }

    @Override
    public ModelDto findById(Long id, Locale lang) {
        return modelRepository.findById(id).orElseThrow(()-> new ModelNotFoundException(messageSource.getMessage("model.not.found.error", null, lang))).map2Dto();
    }

    @Override
    @Transactional
    public ModelDto save(ModelDto modelDto, List<MultipartFile> files, Locale lang) {
        ModelDto modelDto1;
        try {
            modelDto1 = modelRepository.save(modelDto.map2Entity()).map2Dto();
            if (modelDto1 != null) {
                modelDto1.setFileList(fileService.save(null, modelDto1.getModelId(), files, lang));
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(messageSource.getMessage("model.create.error", null, lang));
        }
        return modelDto1;
    }

    @Override
    public List<ModelDto> findAll() {
        List<ModelDto> list = new ArrayList<>();
        modelRepository.findAll().forEach(value->
                list.add(value.map2Dto())
                );
        return list;
    }

    @Override
    public List<ModelDto> findAll(Long productId) {
        List<ModelDto> list = new ArrayList<>();

        modelRepository.findAllByProductId(productId).forEach(value->
                list.add(value.map2Dto())
                );

        return list;
    }

    @Override
    @Transactional
    public ModelDto update(ModelDto modelDto, Long id, List<MultipartFile> files, Locale lang) {
        ModelDto modelDto1 = new ModelDto();
        try {
            if (modelRepository.existsById(id)){
                modelDto1 = modelRepository.save(modelDto.map2Entity()).map2Dto();
                if (modelDto1 != null) {
                    modelDto1.setFileList(fileService.save(null, modelDto1.getModelId(), files, lang));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(messageSource.getMessage("model.update.error", null, lang));
        }
        return modelDto1;
    }

    @Override
    @Transactional
    public boolean deleteByModel(Long modelId, Locale lang) {
        boolean result = false;
        try {
            if (modelRepository.existsById(modelId)){
                modelRepository.deleteById(modelId);
                result = true;
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(messageSource.getMessage("model.delete.error", null, lang));
        }
        return result;
    }

//    @Override
//    @Transactional
//    public boolean deleteByProductId(Long productId) {
//        return false;
//    }
}

package onlineshopping.uz.service.impl;

import lombok.extern.slf4j.Slf4j;
import onlineshopping.uz.model.domain.Region;
import onlineshopping.uz.model.dto.RegionDto;
import onlineshopping.uz.model.dto.UsersDto;
import onlineshopping.uz.repository.RegionRepository;
import onlineshopping.uz.service.DistrictService;
import onlineshopping.uz.service.UsersService;
import org.springframework.stereotype.Service;
import onlineshopping.uz.service.RegionService;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Slf4j
@Service("regionService")
public class RegionServiceImpl implements RegionService {

    private final RegionRepository regionRepository;
    private final DistrictService districtService;
    private final UsersService usersService;

    public RegionServiceImpl(RegionRepository regionRepository, DistrictService districtService, UsersService usersService) {
        this.regionRepository = regionRepository;
        this.districtService = districtService;
        this.usersService = usersService;
    }

    @Override
    @Transactional
    public RegionDto save(RegionDto regionDto, Locale lang) {

        try {
            return regionRepository.save(regionDto.map2Entity()).map2Dto();
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(e.getMessage());
        }
    }

    @Override
    public RegionDto findById(Long id, Locale lang) {
        Optional<Region> region = regionRepository.findById(id);
        return region.map(Region::map2Dto).orElse(null);
    }

    @Override
    public List<RegionDto> findAll() {
        List<RegionDto> regionDtos = new ArrayList<>();
        regionRepository.findAll().forEach(value ->
            regionDtos.add(value.map2Dto())
        );
        return regionDtos;
    }

    @Override
    @Transactional
    public boolean update(RegionDto regionDto, Long id, Locale lang) {
        boolean result = false;
        try {
            if (regionRepository.existsById(id)){
                regionRepository.save(regionDto.map2Entity());
                result = true;
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(e.getMessage());
        }
        return result;
    }

    @Override
    @Transactional
    public boolean delete(Long id, String userName, Locale lang) {
        boolean result = false;
        try {
            if (regionRepository.existsById(id)){
                districtService.deleteByRegionId(id, lang);
                UsersDto usersDto = usersService.findByEmail(userName, lang);
                usersDto.setRegionId(null);
                usersService.update(usersDto, usersDto.getId(), lang);
                regionRepository.deleteById(id);
                result = true;
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalStateException(e.getMessage());
        }
        return result;
    }

}

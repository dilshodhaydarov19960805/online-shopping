package onlineshopping.uz.service.impl;

import onlineshopping.uz.model.domain.File;
import onlineshopping.uz.model.domain.Model;
import onlineshopping.uz.model.domain.Product;
import onlineshopping.uz.repository.FileRepository;
import onlineshopping.uz.service.ModelService;
import onlineshopping.uz.service.ProductService;
import onlineshopping.uz.utils.CustomUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import onlineshopping.uz.service.FileService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Service("fileService")
public class FileServiceImpl implements FileService {

    private final FileRepository fileRepository;
    private final MessageSource messageSource;
    private final ModelService modelService;
    private final ProductService productService;

    @Value("${file.path}")
    private String UPLOAD_FILE_PATH;

    public FileServiceImpl(FileRepository fileRepository, MessageSource messageSource, ModelService modelService, ProductService productService) {
        this.fileRepository = fileRepository;
        this.messageSource = messageSource;
        this.modelService = modelService;
        this.productService = productService;
    }

    @Override
    public void findById(Long id, HttpServletResponse response, Locale lang) {
        try {
                File file = findById(id, lang);
                if (file != null) {
                    response.setContentType(CustomUtils.isNotNullEmpty(file.getContentType()) ? file.getContentType() : "*/*");
                    IOUtils.copy(getFile(file), response.getOutputStream());
                }
            } catch (IOException e){
                e.printStackTrace();
            }
    }

    @Override
    public List<Long> findAllByModelId(Long modelId, Locale lang) {
        Model model = modelService.findById(modelId, lang).map2Entity();
        return fileRepository.findAllByModelId(model);
    }

    @Override
    @Transactional
    public boolean delete(Long id, Locale lang) {
        try {
            File file =  findById(id, lang);
            java.io.File file1 = new java.io.File(file.getUploadPath());
            file1.delete();
            fileRepository.deleteById(id);
            return true;
        }catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    @Transactional
    public List<Long> save(Long productId, Long modelId, List<MultipartFile> multipartFiles, Locale lang) {
        List<Long> list = new ArrayList<>();
        if (!multipartFiles.isEmpty()) {
            multipartFiles.forEach(multipartFile ->{
                File file = new File();
                String fileName = CustomUtils.randomFileName() + "." + getFileType(multipartFile.getOriginalFilename());
                file.setFileName(fileName);
                file.setFileSize(multipartFile.getSize());
                file.setContentType(multipartFile.getContentType());
                file.setType(getFileType(multipartFile.getOriginalFilename()));
                if (productId != null && productId > 0) {
                    file.setProduct(productService.findById(productId, lang).map2Entity());
                } else if (modelId != null && modelId > 0) {
                    file.setModel(modelService.findById(modelId, lang).map2Entity());
                }
                try {
                    file.setUploadPath(fileUploadToPackage(fileName, multipartFile));
                    list.add(file.getId());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        return list;
    }

    @Override
    public List<Long> findByProductId(Product product) {
        return fileRepository.findByProductId(product);
    }

    private String getFileType(String fileName) {
        String type = null;
        if (CustomUtils.isNotNullEmpty(fileName)) {
            int count = fileName.lastIndexOf(".");
            if (count > 0 && count <= fileName.length() - 2) {
                type = fileName.substring(count + 1);
            }
        }
        return type;
    }

    private File findById(Long id, Locale lang) {
        File file = null;
        try {
            file = fileRepository.findById(id).orElseThrow(()-> new FileNotFoundException(messageSource.getMessage("file.not.found.error", null, lang)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return file;
    }

    private InputStream getFile(File file) throws FileNotFoundException {
        java.io.File  file1 =new java.io.File(file.getUploadPath());
        return new FileInputStream(file1);
    }

    private String fileUploadToPackage(String newFileName, MultipartFile multipartFile) throws IOException {
        byte [] bytes = multipartFile.getBytes();
        String fileName = UPLOAD_FILE_PATH + "/" + newFileName;
        Path path = Paths.get(fileName);
        Files.write(path, bytes);
        return fileName;
    }

}

package onlineshopping.uz.service;

import onlineshopping.uz.model.dto.CategoryDto;

import java.util.List;
import java.util.Locale;

public interface CategoryService {

    boolean save(CategoryDto category, Locale locale);

    boolean update(CategoryDto categoryDto, Long id, Locale locale);

    boolean delete(Long id, Locale locale);

    List<CategoryDto> findAll();

    CategoryDto findById(Long id, Locale locale);

}

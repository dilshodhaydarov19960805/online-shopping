package onlineshopping.uz.service;

import onlineshopping.uz.model.dto.BasketDto;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Locale;

public interface BasketService {

    BasketDto findById(Long id, Locale locale);

    ResponseEntity<?> save(BasketDto basket, Locale locale);

    ResponseEntity<?> update(BasketDto basket, Long id, Locale locale);

    boolean delete(Long id, Locale locale);

    List<BasketDto> findAll(Long userId);

}

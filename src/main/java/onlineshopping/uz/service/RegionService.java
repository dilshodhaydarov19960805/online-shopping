package onlineshopping.uz.service;

import onlineshopping.uz.model.dto.RegionDto;

import java.util.List;
import java.util.Locale;

public interface RegionService {

    RegionDto save(RegionDto regionDto, Locale locale);

    RegionDto findById(Long id, Locale locale);

    List<RegionDto> findAll();

    boolean update(RegionDto regionDto, Long id, Locale locale);

    boolean delete(Long id, String userName, Locale locale);

}

package onlineshopping.uz.controller;

import onlineshopping.uz.model.dto.ProductDto;
import onlineshopping.uz.service.ProductService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/api/product")
public class ProductsController {

    private final ProductService productService;

    public ProductsController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public List<ProductDto> getAll(){
        return productService.findAll();
    }

    @GetMapping(path = "/category/{Id}")
    public List<ProductDto> getAllCategoryId(@PathVariable("categoryId") Long categoryId){
        return productService.findAll(categoryId);
    }

    @PostMapping("/save")
    @PreAuthorize(value = "hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> save(@RequestBody ProductDto dto, @RequestParam("files") List<MultipartFile> files, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale lang){
        return new ResponseEntity<>(productService.save(files, dto, lang), HttpStatus.CREATED);
    }

    @PutMapping(path = "/update/{id}")
    @PreAuthorize(value = "hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> update(@PathVariable("id") Long id, @RequestBody ProductDto dto, @RequestParam("files") List<MultipartFile> files, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale){
        return new ResponseEntity<>(productService.update(files, dto, id, locale), HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete/{id}")
    @PreAuthorize(value = "hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale){
        return new ResponseEntity<>(productService.delete(id, locale), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale){
        return new ResponseEntity<>(productService.findById(id, locale), HttpStatus.OK);
    }

}

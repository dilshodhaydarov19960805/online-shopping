package onlineshopping.uz.controller;

import onlineshopping.uz.service.FileService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

@RestController
@RequestMapping("/api/files")
public class FilesController {

    private final FileService fileService;

    public FilesController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping(path = "file/{id}")
    public void getFileById(@PathVariable("id") Long id, HttpServletResponse response, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        fileService.findById(id, response, locale);
    }

//    @PostMapping
//    public void save(@RequestParam("file") MultipartFile file, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale lang) {
//        fileService.save(1L, null, file, lang);
//    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        if (fileService.delete(id, locale)) {
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

}

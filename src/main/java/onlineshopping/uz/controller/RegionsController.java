package onlineshopping.uz.controller;

import onlineshopping.uz.model.dto.RegionDto;
import onlineshopping.uz.security.SecurityUtils;
import onlineshopping.uz.service.RegionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@RestController
@PreAuthorize("hasAnyRole('ADMIN, SUPER_ADMIN')")
@RequestMapping("/api/region")
public class RegionsController {

    private final RegionService regionService;

    public RegionsController(RegionService regionService) {
        this.regionService = regionService;
    }

    @GetMapping
    public List<RegionDto> getAll(){
        return regionService.findAll();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false)Locale locale) {
        return new ResponseEntity<>(regionService.findById(id, locale), HttpStatus.OK);
    }

    @PostMapping(path = "/save")
    public ResponseEntity<?> save(@RequestBody RegionDto dto, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        return new ResponseEntity<>(regionService.save(dto, locale), HttpStatus.CREATED);
    }

    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id") Long id, @RequestBody RegionDto dto, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        return new ResponseEntity<>(regionService.update(dto, id, locale), HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        Optional<String> optional = SecurityUtils.getCurrentUserName();
        return new ResponseEntity<>(regionService.delete(id, optional.get(), locale), HttpStatus.OK);
    }

}

package onlineshopping.uz.controller;

import onlineshopping.uz.model.dto.ModelDto;
import onlineshopping.uz.service.ModelService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/api/model")
public class ModelsController {

    private final ModelService modelService;

    public ModelsController(ModelService modelService) {
        this.modelService = modelService;
    }

    @GetMapping
    public List<ModelDto> getAll() {
        return modelService.findAll();
    }

    @GetMapping(path = "/product/{id}")
    public List<ModelDto> getAllByProductId(@PathVariable("id") Long id) {
        return modelService.findAll(id);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false)Locale locale) {
        return new ResponseEntity<>(modelService.findById(id, locale), HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize(value = "hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> save(@RequestBody ModelDto dto, @RequestParam("files") List<MultipartFile> files, @RequestHeader(name = "lang", defaultValue = "uz", required = false)Locale locale) {
        return new ResponseEntity<>(modelService.save(dto, files, locale), HttpStatus.CREATED);
    }

    @PutMapping(path = "/update/{id}")
    @PreAuthorize(value = "hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> update(@PathVariable("id")Long id, @RequestParam("files") List<MultipartFile> files, @RequestBody ModelDto dto, @RequestHeader(name = "lang", defaultValue = "uz", required = false)Locale locale) {
        return new ResponseEntity<>(modelService.update(dto, id, files, locale), HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete/{id}")
    @PreAuthorize(value = "hasAnyRole('ADMIN', 'SUPER_ADMIN')")
    public ResponseEntity<?> delete(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false)Locale locale) {
        return new ResponseEntity<>(modelService.deleteByModel(id, locale), HttpStatus.OK);
    }

}

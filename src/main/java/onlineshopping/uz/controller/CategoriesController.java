package onlineshopping.uz.controller;

import onlineshopping.uz.model.dto.CategoryDto;
import onlineshopping.uz.service.CategoryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/api/categories")
public class CategoriesController {

    private final CategoryService categoryService;

    public CategoriesController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public List<CategoryDto> getAll(){
        return categoryService.findAll();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false)Locale lang){
        return new ResponseEntity<>(categoryService.findById(id, lang), HttpStatus.OK);
    }

    @PostMapping(path = "/save")
    public ResponseEntity<?> save(@RequestBody CategoryDto dto, @RequestHeader(name = "lang", defaultValue = "uz", required = false)Locale lang){
        if (!categoryService.save(dto, lang)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id") Long id, @RequestBody CategoryDto dto, @RequestHeader(name = "lang", defaultValue = "uz", required = false)Locale lang){
        if (!categoryService.update(dto, id, lang)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false)Locale lang){
        if (!categoryService.delete(id, lang)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

}

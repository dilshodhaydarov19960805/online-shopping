package onlineshopping.uz.controller;

import onlineshopping.uz.model.dto.UsersDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import onlineshopping.uz.model.domain.Users;
import onlineshopping.uz.security.SecurityUtils;
import onlineshopping.uz.service.UsersService;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UsersService usersService;

    public UserController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN, SUPER_ADMIN')")
    public List<Users> getAll() {
        Optional<String> optional = SecurityUtils.getCurrentUserName();
        optional.ifPresent(System.out::println);
        return usersService.getAll();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale lang) {
        if (!usersService.delete(id, lang)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<?> update(@PathVariable("id") Long id, @RequestBody UsersDto dto, @RequestHeader(name = "lang", defaultValue = "uz", required = false)Locale lang) {
        if (!usersService.update(dto, id, lang)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getUserById(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false)Locale lang) {
        return new ResponseEntity<>(usersService.findById(id, lang), HttpStatus.OK);
    }

}

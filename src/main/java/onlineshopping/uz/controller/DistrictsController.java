package onlineshopping.uz.controller;

import onlineshopping.uz.model.dto.DistrictDto;
import onlineshopping.uz.service.DistrictService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;

@RestController
@RequestMapping("/api/districts")
@PreAuthorize("hasAnyRole('ADMIN, SUPER_ADMIN')")
public class DistrictsController {

    private final DistrictService districtService;

    public DistrictsController(DistrictService districtService) {
        this.districtService = districtService;
    }

    @GetMapping(path = "/region/{id}")
    public List<DistrictDto> getAll(@PathVariable("id") Long regionId, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        return districtService.findAll(regionId);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        return new ResponseEntity<>(districtService.findById(id, locale), HttpStatus.OK);
    }

    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id") Long id,@RequestBody DistrictDto dto, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        return new ResponseEntity<>(districtService.update(dto, id, locale), HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        return new ResponseEntity<>(districtService.delete(id, locale), HttpStatus.OK);
    }

    @PostMapping(path = "save")
    public ResponseEntity<?> save(@RequestBody DistrictDto dto, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        return new ResponseEntity<>(districtService.save(dto, locale),HttpStatus.CREATED);
    }

}

package onlineshopping.uz.controller;

import onlineshopping.uz.model.dto.BasketDto;
import onlineshopping.uz.model.dto.UsersDto;
import onlineshopping.uz.security.SecurityUtils;
import onlineshopping.uz.service.BasketService;
import onlineshopping.uz.service.UsersService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

@RestController
@RequestMapping("/api/baskets")
public class BasketsController {

    private final BasketService basketService;
    private final UsersService usersService;

    public BasketsController(BasketService basketService, UsersService usersService) {
        this.basketService = basketService;
        this.usersService = usersService;
    }

    @GetMapping
    public List<BasketDto> getAll(@RequestHeader(name = "lang", defaultValue = "uz", required = false)Locale locale) {
        Optional<String> optional = SecurityUtils.getCurrentUserName();
        UsersDto usersDto = usersService.findByEmail(optional.get(), locale);
        return basketService.findAll(usersDto.getId());
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        return new ResponseEntity<>(basketService.findById(id, locale), HttpStatus.OK);
    }

    @PutMapping(path = "/update/{id}")
    public ResponseEntity<?> update(@PathVariable("id") Long id,@RequestBody BasketDto dto, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        return new ResponseEntity<>(basketService.update(dto, id, locale), HttpStatus.OK);
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity<?> delete(@PathVariable("id") Long id, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        return new ResponseEntity<>(basketService.delete(id, locale), HttpStatus.OK);
    }

    @PostMapping(path = "save")
    public ResponseEntity<?> save(@RequestBody BasketDto dto, @RequestHeader(name = "lang", defaultValue = "uz", required = false) Locale locale) {
        return new ResponseEntity<>(basketService.save(dto, locale),HttpStatus.CREATED);
    }

}

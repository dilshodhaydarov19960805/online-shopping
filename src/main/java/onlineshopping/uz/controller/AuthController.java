package onlineshopping.uz.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import onlineshopping.uz.model.UserLogin;
import onlineshopping.uz.model.dto.UsersDto;
import onlineshopping.uz.service.UsersService;

import javax.validation.Valid;
import java.util.Locale;

@RestController
@RequestMapping(path = "/api/v1/")
public class AuthController {

    @Autowired
    private UsersService usersService;

    @PostMapping(path = "/login")
    public ResponseEntity<?> loginUser(@Valid @RequestBody UserLogin login, @RequestHeader(name = "lang", defaultValue = "uz", required = false)Locale locale){
        return usersService.loginUser(login, locale);
    }

    @PostMapping(path = "/register")
    public ResponseEntity<?> register(@Valid @RequestBody UsersDto usersDto){
        return usersService.save(usersDto);
    }

}

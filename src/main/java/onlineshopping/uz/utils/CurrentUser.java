package onlineshopping.uz.utils;

import org.springframework.security.core.annotation.AuthenticationPrincipal;

@AuthenticationPrincipal
public @interface CurrentUser {

}

package onlineshopping.uz.utils;

import org.apache.commons.lang3.RandomStringUtils;

public class CustomUtils {

    private static int randomFileNameCount = 24;
    public static boolean isNotNullEmpty(String s){
        return (s != null && !s.equals("null")) && !s.trim().isEmpty();
    }

    public static String randomFileName() {
        String random = RandomStringUtils.random(randomFileNameCount, "qwertyuiopasdfghjklzxcvbnm1234567890");
        return random;
    }

}
